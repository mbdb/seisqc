# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 11:39:17 2020

@author: maxime
"""
from obspy import read, Stream
from os import makedirs

from seisqc_utils import get_days_2_process, jday2UTCDateTime, \
    list_db_files, parse_jday_from_string


def make_images_trace(chan, depth, conf):
    """
    Creates all images from traces recorded by process_trace.
    Write them in tree like:
        html_path/net/sta/loc/chan.depth.trace.png
    """
    days = get_days_2_process(depth)
    files = list_db_files(chan, days[0], days[-1], 'trace.mseed', conf)

    png_outpath = "%s/%s/%s/%s" % (conf['main']['html_path'],
                                   chan[0], chan[1], chan[2])
    png_outfile = "%s/%s.%s.trace.png" % (png_outpath, chan[3], depth)
    makedirs(png_outpath, exist_ok=True)
    files.sort()
    if len(files) > 0:
        st = Stream()
        for f in files:
            st += read(f)
        st.merge()
        if len(st) == 1:
            st.plot(outfile=png_outfile, size=(640, 200))


def process_trace(st, file):
    """
    Create resampled trace and save it in mseed file.
    """
    st0 = st.copy()
    start = jday2UTCDateTime(parse_jday_from_string(file))
    end = start + 86400
    st0.merge()
    st0.trim(start, end)
    st0 = st0.split()
    if len(st0) > 0:
        for sr in [100, 10, 1]:
            if st0[0].stats.sampling_rate > sr:
                st0.detrend('demean')
                st0.resample(sr, no_filter=False)
        st0.write(file, format="MSEED", encoding=5)
