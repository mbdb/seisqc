#!/usr/bin/env sh
for f in /root/docker-entrypoint-init.d/*
do
    if [ -f "$f" ]
    then
    	echo "$0: running $f"; . "$f"
    fi
done
lighttpd -D -f /etc/lighttpd/lighttpd.conf