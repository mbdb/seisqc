# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 10:32:44 2020

@author: maxime
"""
from copy import deepcopy
import glob
import numpy as np
from obspy import UTCDateTime
import os
import shutil


def get_chan_2_process(conf):
    """
    Iterate on conf dict to get channels to process and their fdsnws url.
    Returns a list of channels and their fdsnws url.
    Ex:
        [['G', 'CCD', '00', 'BHZ', 'IPGP'],
         ['FR', 'BOUC', '00', 'HHZ', 'RESIF']]
    """
    ch2process = list()
    source = conf['main']['url']
    for net in conf['networks'].keys():
        sta_list = deepcopy(conf['networks'][net])
        n_source = source
        if 'url' in sta_list.keys():
            n_source = conf['networks'][net]['url']
            sta_list.pop('url')
        for sta in sta_list.keys():
            loc_list = deepcopy(conf['networks'][net][sta])
            s_source = n_source
            if 'url' in loc_list.keys():
                s_source = conf['networks'][net][sta]['url']
                loc_list.pop('url')
            for loc in loc_list.keys():
                cha_list = deepcopy(conf['networks'][net][sta][loc])
                test_list = [isinstance(cha, dict) for cha in cha_list]
                l_source = s_source
                if any(test_list):
                    l_source = cha_list[test_list.index(True)]['url']
                    cha_list.pop(test_list.index(True))
                for cha in cha_list:
                    ch2process.append([net, sta, loc, cha, l_source])
    return ch2process


def get_days_2_process(depth):
    """
    Parse julian days to process, according to depth.
    Returns a list of days.
    Ex:
       ['2020.141', '2020.142', '2020.143']
    """
    utcnow = UTCDateTime.utcnow()
    end = jday2UTCDateTime(UTCDateTime2jday(utcnow))
    start = end - depth*86400
    days2process = list()
    t = start
    while t < end:
        days2process.append("%d.%03d" % (t.year, t.julday))
        t += 86400
    return days2process


def get_remaining_2_process(chan, n_process, conf, max_depth):
    """
    Renvoie liste des jours à traiter, si - de n_process fichiers par
    jour sont présents
    """
    days2process = get_days_2_process(max_depth)
    chan_id = "%s.%s.%s.%s" % (chan[0], chan[1], chan[2], chan[3])
    fid2process = list()
    for day in days2process:
        fid2process.append("%s.%s" % (chan_id, day))
    remaining = list()
    for fid in fid2process:
        search_path = "%s*" % os.path.join(conf['main']['db_path'],
                                           fid)
        if len(glob.glob(search_path)) < n_process:
            remaining.append(fid)
    return remaining


def list_db_files(chan, start, end, suffix, conf):
    """
    Search files in db of specific channel, time range and suffix
    Returns a list of files
    Ex:
        ['/var/lib/seisqc/db/G.CCD.00.BHZ.2020.141.ppsd.npz',
         '/var/lib/seisqc/db/G.CCD.00.BHZ.2020.142.ppsd.npz']
    """
    t = jday2UTCDateTime(start)
    files = list()
    db_path = conf['main']['db_path']
    while t <= jday2UTCDateTime(end):
        file = "%s/%s.%s.%s.%s.%d.%03d.%s" % (db_path,
                                              chan[0], chan[1],
                                              chan[2], chan[3],
                                              t.year, t.julday,
                                              suffix)
        t += 86400
        if os.path.isfile(file):
            files.append(file)
    return files


def jday2UTCDateTime(day):
    """
    Returns a UTCDateTime object from a string julian day
    """
    return UTCDateTime(day.replace('.', ''))


def parse_chan_from_string(string):
    """
    Returns channel id from file name with following pattern:
        net.sta.loc.cha.year.day
    Ex:
        'G.CCD.00.BHZ'
    """
    return "%s.%s.%s.%s" % (string.split('.')[0], string.split('.')[1],
                            string.split('.')[2], string.split('.')[3])


def parse_jday_from_string(string):
    """
    Return julian day from string with following pattern:
        net.sta.loc.cha.year.day
    Ex:
        '2020.141'
    """
    return "%s.%s" % (string.split('.')[4], string.split('.')[5])


def purge_html_path(conf):
    """
    Purge all files or dirs in html path
    """
    for d in glob.glob("%s/*" % conf['main']['html_path']):
        if os.path.isdir(d):
            shutil.rmtree(d)
        if os.path.isfile(d):
            os.remove(d)


def rm_old_db_files(conf, max_depth):
    """
    Remove old files from db dir, according to the max depth
    """
    days_list = get_days_2_process(max_depth)
    with os.scandir(conf['main']['db_path']) as it:
        for entry in it:
            if entry.is_file():
                file_day = parse_jday_from_string(entry.name)
                if file_day not in days_list:
                    print("%s: removed from db" % entry.name)
                    os.remove(os.path.join(conf['main']['db_path'],
                                           entry.name))


def rm_other_db_files(conf):
    """
    Remove files from db belonging to other channels than those to process
    """
    tmp_chan_list = get_chan_2_process(conf)
    chan_list = list()
    for chan in tmp_chan_list:
        chan_list.append("%s.%s.%s.%s"
                         % (chan[0], chan[1], chan[2], chan[3]))
    with os.scandir(conf['main']['db_path']) as it:
        for entry in it:
            if entry.is_file():
                file_chan = parse_chan_from_string(entry.name)
                if file_chan not in chan_list:
                    print("%s: removed from db" % entry.name)
                    os.remove(os.path.join(conf['main']['db_path'],
                                           entry.name))


def UTCDateTime2jday(time):
    """
    Returns a string julian day from an UTCDateTime object
    """
    return "%d.%03d" % (time.year, time.julday)
