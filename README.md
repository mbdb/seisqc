# seisqc

## Installation and getting started

1. Edit conf.yaml and pay attention to products directories (html_path,
templates_path and db_path).
2. Create and give permissions to those directories, ex:  
    ```~$ sudo mkdir -p /var/lib/seisqc/db```  
    ```~$ sudo mkdir -p /var/lib/seisqc/html```  
    ```~$ sudo mkdir -p /var/lib/seisqc/templates```  
    ```~$ sudo chown -R user:user /var/lib/seisqc```  
3. Copy template files in templates directory:  
    ```~$ cp templates/*html -/var/lib/seisqc/templates```  
4. Install requirements:  
    ```~$ python3 -m pip install -r requirements.txt```  
5. Run seisqc:  
    ```~$ python3 seisqc.py conf.yaml```  
Data are downloaded and processed, results are stored in db_path. If seisqc.py
is run again, db_path is scanned to avoid processing already processed time
spans. At the end, html files are generated in html_path.
6. Install seisqc in crontab, to update hmtl files daily.

## Use with docker

1. Build seisqc image, ex:  
    ```~$ docker build -t seisqc:latest .```  
2. Create etc directory and modify conf.yaml in it. Leave default path (db,
templates and html), and configure your own network path.  
3. Run docker image, open port 80 and mount your host's etc in container's
/etc/seisqc. Ex:  
    ```~$ docker run -p 80:80 --name seisqc -v ./etc:/etc/seisqc seisqc:latest```  
4. Run seisqc.py regularly within container:  
    ```~$ docker exec -i seisqc:latest python3 /opt/seisqc/seisq.py /var/lib/seisqc/etc/conf.yaml```  
5. Host's ./etc/conf.yaml remains editable any time.  

## Seisqc workflow

1. Reads config from conf file (yaml format) in ```Seisqc.__init__```  
A sample is provided, please note following tips about this file:
    - tags under 'main', 'ppsd' and 'detections' are mandatory
    - url tag under 'main' provides a default fdsnws url (mandatory)
    - url tag under networks (either at net, sta or loc level) gives a
      specific fdsnurl which overwrites default url, only for channels at lower
      level
    - url tag is not allowed at channel level, lowest level is location
    - depths list contains the depths to compute images in days
    - depths list handles some keywords:
        + day (1)
        + week (7)
        + month (30)
        + year (365)
2. ```Seisqc.rm_db_files``` function removes all unuseful files from db path,
either because they're too old, or because they come from an other channel than
those to compute. It keeps db dir clean, with only useful files, according to
networks and depths defined in conf file.
3. ```Seisqc.purge_db_files``` function removes all files from db path.
4. ```Seisqc.process``` function launches all needed processes. It runs a loop
calling private function ```__process_day``` performing all processes for a
chan/day:
    - ```__process_day``` gets data and metadata of a specific chan/day (8 hours
      more at each end), and launches all ```process_*``` functions
    - each process has its own dedicated function called process_name, having
      a day stream, metadata and an output file name as argument
    - each process must write data in db path. This is mandatory as it is later
      used to know what data needs to be downloaded again.
      The db file name must follow the pattern:
        net.sta.loc.cha.year.jday.suffix (suffix being an open choice, but
        unique for a given process)
This structure makes the computation by day mandatory, but allows new processes
integration in the future. Take example on process_ppsd function.
5. ```Seisqc.make_images``` function follows the same philosophy. It runs a loop
calling ```__make_images_*``` for a specific chan and depth. Each ```__make_images_*``` function reads needed db files for chan/depth previously
written by ```__process_*```, creates images in png format, and writes them in html_path following the pattern:
    html_path/net/sta/loc/chan.depth.type.png (type being an open choice, but unique for a given product).
