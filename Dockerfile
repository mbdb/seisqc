FROM alpine:latest

ENV SEISQC_ROOT=/opt/seisqc
ENV SEISQC_LIB=/var/lib/seisqc
ENV SEISQC_ETC=/etc/seisqc
ENV SEISQC_LOG=/var/log

RUN apk add gcc \
    git \
    libc-dev \
    lighttpd \
    python3

RUN mkdir -p $SEISQC_ROOT \
    && mkdir -p $SEISQC_LIB/db \
    && mkdir $SEISQC_LIB/templates \
    && mkdir -p $SEISQC_ETC \
    && mkdir -p /var/log/lighttpd \
    && mkdir -p /var/lib/lighttpd

RUN chown -R lighttpd:lighttpd /var/log/lighttpd \
    && chown -R lighttpd:lighttpd /var/lib/lighttpd

COPY *.py requirements.txt $SEISQC_ROOT
COPY conf.yaml $SEISQC_ETC
COPY templates/* $SEISQC_LIB/templates

RUN python3 -m venv $SEISQC_ROOT \
    && source $SEISQC_ROOT/bin/activate \
    && python3 -m pip install -U pip \
    && python3 -m pip install -r $SEISQC_ROOT/requirements.txt

RUN echo "33 3 * * * $SEISQC_ROOT/bin/python $SEISQC_ROOT/seisqc.py $SEISQC_ETC/conf.yaml > $SEISQC_LOG/seisqc.log 2>&1" >> $SEISQC_ROOT/seisqc.sh \
    && crontab $SEISQC_ROOT/seisqc.sh

RUN mkdir /root/docker-entrypoint-init.d

COPY --chmod=700 docker-entrypoint.sh /root

ENTRYPOINT ["/root/docker-entrypoint.sh"]
