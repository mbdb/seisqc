# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 11:39:17 2020

@author: maxime
"""
import json
from matplotlib import dates, pyplot
import os

from seisqc_utils import get_days_2_process, jday2UTCDateTime, \
    list_db_files, parse_jday_from_string


def make_images_availability(chan, depth, conf):
    """
    Creates all images created by __process_availability.
    Write them in tree like:
        html_path/net/sta/loc/chan.depth.availability.png
    """
    # List days to compute and available files, prepare output paths
    days = get_days_2_process(depth)
    files = list_db_files(chan, days[0], days[-1], 'availability.json', conf)

    png_outpath = "%s/%s/%s/%s" % (conf['main']['html_path'],
                                   chan[0], chan[1], chan[2])
    try:
        os.makedirs(png_outpath)
    except FileExistsError:
        pass

    # Calc. start and end time from days list
    start = jday2UTCDateTime(days[0])
    end = jday2UTCDateTime(days[-1]) + 86400
    start = start.matplotlib_date
    end = end.matplotlib_date

    # Retrieve detected gaps list
    overall_gaps = list()
    for file in files:
        with open(file) as infile:
            gaps = json.load(infile)
        [overall_gaps.append(g) for g in gaps]

    # Retrieve not detected gaps list, becouse there is no file
    db_path = conf['main']['db_path']
    for day in days:
        expected_file = "%s/%s.%s.%s.%s.%s.%s.%s" % (db_path,
                                                     chan[0], chan[1],
                                                     chan[2], chan[3],
                                                     day.split('.')[0],
                                                     day.split('.')[1],
                                                     'availability.json')
        if expected_file not in files:
            gap = list()
            [gap.append(cid) for cid in chan[0:4]]
            t0 = jday2UTCDateTime(day)
            t1 = t0 + 86400
            gap.append(t0.matplotlib_date)
            gap.append(t1.matplotlib_date)
            gap.append(86400)
            overall_gaps.append(gap)
    overall_gaps.sort()

    # Retrieve time series boundaries from overall gaps list
    time_series = list()
    lost = 0
    previous_t = start
    for g in overall_gaps:
        time_series.append([previous_t, g[4]])
        previous_t = g[5]
        lost += g[4] - g[5]
    time_series.append([previous_t, end])
    lost = lost*100 / (start-end)

    # Make figure
    fig = pyplot.figure()
    start = dates.num2date(start).strftime("%Y-%m-%d")
    end = dates.num2date(end).strftime("%Y-%m-%d")
    fig.suptitle("%s.%s.%s.%s: %s-%s" % (chan[0], chan[1],
                                         chan[2], chan[3],
                                         days[0], days[-1]))
    ax = fig.gca()
    for ts in time_series:
        ax.vlines([ts[0], ts[1]], ymin=-1, ymax=1, colors='r')
        ax.hlines(0, xmin=ts[0], xmax=ts[1], colors='b')
    ax.xaxis_date()
    ax.set_yticks([0])
    ax.set_yticklabels(["%.1f percent" % (100-lost)], rotation='vertical')
    ax.grid()
    fig.autofmt_xdate()
    png_outfile = "%s/%s.%s.availability.png" % (png_outpath, chan[3],
                                                 depth)
    fig.savefig(png_outfile)
    pyplot.close(fig)

def process_availability(st, file):
    """
    Process availability from stream using get_gaps func, save results in
    json file. Keep all dates in matplotlib format
    """
    start = jday2UTCDateTime(parse_jday_from_string(file))
    end = start + 86400
    all_gaps = st.get_gaps()
    kept_gaps = list()
    for g in all_gaps:
        if g[4] > start and g[4] < end:
            kept_gaps.append(g)
    for g in kept_gaps:
        g[4] = g[4].matplotlib_date
        g[5] = g[5].matplotlib_date
    # Does not write any file if there is no data (therefore no gap), but
    # writes an empty file if all data there (therefore no gap)
    st0 = st.copy()
    st0.trim(start, end)
    if len(st0) > 0:
        with open(file, 'w') as outfile:
            json.dump(kept_gaps, outfile)
