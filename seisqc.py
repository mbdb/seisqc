#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
class Seisqc

1. Reads config from conf file (yaml format) in __init__
A sample is provided, please note following tips about this file:
    - tags under 'main', 'ppsd' and 'detections' are mandatory
    - url tag under main provides a default fdsnws url (mandatory)
    - url tag under networks (either at net, sta or loc level) gives a
      specific fdsnurl which overwrites default url, only for channels at lower
      level
    - url tag is not allowed at channel level, lowest level is location
    - depths list contains the depths to compute images in days
    - depths list handles some keywords:
        day (1)
        week (7)
        month (30)
        year (365)

2. Seisqc.rm_db_files function removes all unuseful files from db path, either
because they're too old, or because they come from an other channel than those
to compute. It keeps db dir clean, with only useful files, according to
networks and depths defined in conf file.

3. Seisqc.purge_db_files function removes all files from db path.

4. Seisqc.process function launches all needed processes. It runs a loop
calling private function __process_day performing all processes for a chan/day:
    - __process_day gets data and metadata of a specific chan/day (8 hours more
      at each end), and launches all process_* functions
    - each process has its own dedicated function called process_name, having
      a day stream, metadata and an output file name as argument
    - each process must write data in db path. This is mandatory as it is later
      used to know what data needs to be downloaded again.
      The db file name must follow the pattern:
        net.sta.loc.cha.year.jday.suffix (suffix being an open choice, but
        unique for a given process)
This structure makes the computation by day mandatory, but allows new processes
integration in the future. Take example on process_ppsd function.

5. Seisqc.make_images function follows the same philosophy. It runs a loop
calling __make_images_* for a specific chan and depth. Each __make_images_*
function reads needed db files for chan/depth previously written by
__process_*, creates images in png format, and writes them in html_path
following the pattern:
    html_path/net/sta/loc/chan.depth.type.png (type being an open choice, but
    unique for a given product)
"""
import argparse
from jinja2 import Template
import matplotlib
from multiprocessing.pool import ThreadPool
from obspy.clients.fdsn import Client
from obspy.clients.fdsn.header import FDSNNoDataException
from obspy.io.mseed import InternalMSEEDError
import os
import shutil
import yaml

matplotlib.use('AGG')

from seisqc_utils import get_chan_2_process, get_remaining_2_process, \
    jday2UTCDateTime, parse_chan_from_string, parse_jday_from_string, \
    purge_html_path, rm_old_db_files, rm_other_db_files

from cmp_trace import make_images_trace, process_trace
from cmp_availability import make_images_availability, process_availability
from cmp_detections import make_images_detections, process_detections
from cmp_ppsd import make_images_ppsd, process_ppsd


class Seisqc:

    def __init__(self, conf_file):
        # Open and parse conf file
        f = open(conf_file)
        self.conf = yaml.load(f, Loader=yaml.SafeLoader)
        f.close()

        # Define max depth
        self.depths = dict()
        # Handles keyword
        for d in self.conf['main']['depths']:
            if d == 'yesterday':
                self.depths['yesterday'] = 1
            elif d == 'week':
                self.depths['week'] = 7
            elif d == 'month':
                self.depths['month'] = 30
            elif d == 'year':
                self.depths['year'] = 365
            else:
                self.depths[str(d)] = int(d)
        self.max_depth = int(max(list(self.depths.values())))

        # Jinja2 templates
        with open(os.path.join(self.conf['main']['templates_path'],
                               'index.html'),
                  'r') as f:
            self.index_template = f.read()
        with open(os.path.join(self.conf['main']['templates_path'],
                               'img.html'),
                  'r') as f:
            self.img_page_template = f.read()

    def create_website(self):
        """
        Scans html dir, creates links in html file to be able to explore tree.
        When encounters images, creates html file for each depth.
        """
        print("Building web pages")
        shutil.copy(os.path.join(self.conf['main']['templates_path'],
                                 'sliding_panel.css'),
                    self.conf['main']['html_path'])
        for path, dirs, files in os.walk(self.conf['main']['html_path']):
            if len(files) > 0 and path is not self.conf['main']['html_path']:
                for depth in self.depths:
                    chan_processed = list()
                    trace_files = list()
                    availability_files = list()
                    detection_files = list()
                    ppsd_files = list()
                    spectrogram_files = list()
                    evolution_files = list()
                    for f in files:
                        if f.split('.')[1] == str(self.depths[depth]):
                            if f.split('.')[0] not in chan_processed:
                                chan_processed.append(f.split('.')[0])
                            if f.split('.')[2] == 'trace':
                                trace_files.append(f)
                            elif f.split('.')[2] == 'availability':
                                availability_files.append(f)
                            elif f.split('.')[2] == 'detections':
                                detection_files.append(f)
                            elif f.split('.')[2] == 'ppsd':
                                ppsd_files.append(f)
                            elif f.split('.')[2] == 'spectrogram':
                                spectrogram_files.append(f)
                            elif f.split('.')[2] == 'temporal':
                                evolution_files.append(f)
                    [_list.sort() for _list in [chan_processed,
                                                trace_files,
                                                availability_files,
                                                detection_files,
                                                ppsd_files,
                                                spectrogram_files,
                                                evolution_files]]
                    template = Template(self.img_page_template)
                    page = template.render(chans=chan_processed,
                                           tra_files=trace_files,
                                           ava_files=availability_files,
                                           det_files=detection_files,
                                           pps_files=ppsd_files,
                                           spe_files=spectrogram_files,
                                           evo_files=evolution_files)
                    output_html = os.path.join(path, '%s.html' % depth)
                    with open(output_html, 'w') as f:
                        f.write(page)
        template = Template(self.index_template)
        page = template.render(networks=self.conf['networks'],
                               depths=self.depths)
        output_html = os.path.join(self.conf['main']['html_path'],
                                   'index.html')
        with open(output_html, 'w') as f:
            f.write(page)

    def make_images(self):
        """
        Creates all output images by calling all __make_images_* functions
        """
        print("Building images")
        purge_html_path(self.conf)
        for chan in get_chan_2_process(self.conf):
            for depth in self.depths.keys():
                # availability plotting
                make_images_trace(chan, self.depths[depth], self.conf)
                # availability plotting
                make_images_availability(chan, self.depths[depth], self.conf)
                # detections plotting
                make_images_detections(chan, self.depths[depth], self.conf)
                # ppsd plotting
                make_images_ppsd(chan, self.depths[depth], self.conf)

    def process(self):
        """
        Main process function, launch __process_chan_day (for each chan and
        day) in a multi-processing pool.
        """
        pool = ThreadPool(os.cpu_count()-1)
        for chan in get_chan_2_process(self.conf):
            ws = Client(base_url=chan[-1])
            for fid in get_remaining_2_process(chan, 4, self.conf,
                                               self.max_depth):
                pool.apply_async(self.process_chan_day, args=(ws, fid))
        pool.close()
        pool.join()

    def process_chan_day(self, ws, fid):
        """
        Perform all processes for a specific day and chan:
            - get metadata
            - get data
            - process by calling all process_* functions
        Param days and chan are deduced from a file id with following pattern:
            net.sta.loc.chan.year.jday
        """
        jday = parse_jday_from_string(fid)
        start = jday2UTCDateTime(jday)
        chan = parse_chan_from_string(fid).split('.')
        end = start + 86400
        start -= 28800
        abs_fid = os.path.join(self.conf['main']['db_path'], fid)
        try:
            print("%s: fetch metadata" % (fid))
            # Get metadata
            inv = ws.get_stations(network=chan[0], station=chan[1],
                                  location=chan[2], channel=chan[3],
                                  startbefore=start, endafter=end,
                                  level='response')
            # Get data of day
            print("%s: fetch data" % (fid))
            st = ws.get_waveforms(chan[0], chan[1], chan[2], chan[3],
                                  start, end)
            # Launch processes for day and chan
            print("%s: resample trace" % (fid))
            process_trace(st, "%s.trace.mseed" % abs_fid)
            print("%s: scanning availability" % (fid))
            process_availability(st, "%s.availability.json" % abs_fid)
            print("%s: processing detections" % (fid))
            process_detections(st, inv,
                               "%s.detections.npz" % abs_fid, self.conf)
            print("%s: processing ppsd" % (fid))
            process_ppsd(st, inv, "%s.ppsd.npz" % abs_fid, self.conf)
        except FDSNNoDataException:
            print("%s: no [meta]data available" % (fid))
        except InternalMSEEDError:
            print("%s: corrupted data" % (fid))
        except ConnectionResetError:
            print("%s: connection reset" % (fid))

    def purge_db_files(self):
        """
        Purge all files or dirs from db path
        """
        for entry in os.listdir(self.conf['main']['db_path']):
            abspath = os.path.join(self.conf['main']['db_path'], entry)
            if os.path.isfile(abspath):
                os.remove(abspath)
            if os.path.isdir(abspath):
                shutil.rmtree(abspath)

    def rm_db_files(self):
        """
        Remove all unuseful files from db path
        """
        rm_old_db_files(self.conf, self.max_depth)
        rm_other_db_files(self.conf)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('conf')
    args = parser.parse_args()
    qc = Seisqc(args.conf)
    qc.rm_db_files()
    qc.process()
    qc.make_images()
    qc.create_website()
