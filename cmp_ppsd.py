# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 11:27:39 2020

@author: maxime
"""
from matplotlib import pyplot
from obspy.signal import PPSD
import os

from seisqc_utils import get_days_2_process, jday2UTCDateTime, \
    list_db_files, parse_jday_from_string


def make_images_ppsd(chan, depth, conf):
    """
    Creates all images created by __process_ppsd.
    Write them in tree like:
        html_path/net/sta/loc/chan.depth.type.png
    Types are:
        - ppsd
        - temporal
    """
    days = get_days_2_process(depth)
    files = list_db_files(chan, days[0], days[-1], 'ppsd.npz', conf)
    png_outpath = "%s/%s/%s/%s" % (conf['main']['html_path'],
                                   chan[0], chan[1], chan[2])
    try:
        os.makedirs(png_outpath)
    except FileExistsError:
        pass
    files.sort()
    if len(files) > 0:
        for file in files:
            try:
                ppsd.add_npz(file)
            except (NameError, AssertionError):
                ppsd = PPSD.load_npz("%s" % (file))
        png_outfile = "%s/%s.%s.ppsd.png" % (png_outpath, chan[3], depth)
        fig = ppsd.plot(period_lim=(ppsd.psd_periods[0],
                                    ppsd.psd_periods[-1]),
                        show_percentiles=True, percentiles=[25,50,75],
                        show=False)
        fig.axes[0].set_ylim(-200, -80)
        fig.savefig(png_outfile)
        pyplot.close(fig)

        png_outfile = "%s/%s.%s.spectrogram.png" % (png_outpath, chan[3],
                                                    depth)
        fig = ppsd.plot_spectrogram(clim=[-200, -80], show=False)
        fig.suptitle("%s.%s.%s.%s: %s-%s" % (chan[0], chan[1],
                                             chan[2], chan[3],
                                             days[0], days[-1]))
        ax = fig.gca()
        fig.savefig(png_outfile)
        pyplot.close(fig)

        png_outfile = "%s/%s.%s.temporal.png" % (png_outpath, chan[3],
                                                 depth)
        fig = ppsd.plot_temporal(period=[0.1, 5, 100], show=False)
        fig.suptitle("%s.%s.%s.%s: %s-%s" % (chan[0], chan[1],
                                             chan[2], chan[3],
                                             days[0], days[-1]))
        ax = fig.gca()
        ax.set_ylim(-200, -80)
        fig.savefig(png_outfile)
        pyplot.close(fig)


def process_ppsd(st, inv, file, conf):
    """
    Process ppsd from stream and inventory, save results in file
    """
    st0 = st.copy()
    start = jday2UTCDateTime(parse_jday_from_string(file))
    end = start + 86400
    p_length = int(conf['ppsd']['psd_length'])
    p_overlap = float(conf['ppsd']['overlap'])
    start -= p_length*p_overlap
    st0.trim(start, end-1)
    if len(st0) > 0:
        ppsd = PPSD(st0[0].stats, metadata=inv, ppsd_length=p_length,
                    overlap=p_overlap, db_bins=(-200,-80,1))
        ppsd.add(st0)
        ppsd.save_npz(file)
    else:
        print("%s: no [meta]data available" % (file.split('/')[-1]))
