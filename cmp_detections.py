# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 14:23:26 2020

@author: maxime
"""
from matplotlib import pyplot
import numpy as np
from obspy.signal.trigger import classic_sta_lta, trigger_onset
import os

from seisqc_utils import get_days_2_process, jday2UTCDateTime, list_db_files, \
    parse_jday_from_string


def make_images_detections(chan, depth, conf):
    """
    Creates all images created by process_detections.
    Write them in tree like:
        html_path/net/sta/loc/chan.depth.detections.png
    """
    days = get_days_2_process(depth)
    start = jday2UTCDateTime(days[0])
    files = list_db_files(chan, days[0], days[-1], 'detections.npz', conf)

    png_outpath = "%s/%s/%s/%s" % (conf['main']['html_path'],
                                   chan[0], chan[1], chan[2])
    try:
        os.makedirs(png_outpath)
    except FileExistsError:
        pass
    files.sort()
    if len(files) > 0:
        trigs = np.array([])
        for file in files:
            loaded = np.load(file)
            trigs = np.append(trigs, loaded['triggers'])
        times = np.arange(0, trigs.size/24, 1./24, dtype=np.float64)
        times += start.matplotlib_date
        fig = pyplot.figure()
        fig.suptitle("%s.%s.%s.%s: %s-%s" % (chan[0], chan[1],
                                             chan[2], chan[3],
                                             days[0], days[-1]))
        ax = fig.gca()
        ax.bar(times, trigs, width=1. / 24)
        ax.set_xlim(times[0], times[-1])
        ax.xaxis_date()
        ax.grid()
        ax.set_ylabel("Detections [#/hour]")
        fig.autofmt_xdate()
        png_outfile = "%s/%s.%s.detections.png" % (png_outpath, chan[3], depth)
        fig.savefig(png_outfile)
        pyplot.close(fig)


def process_detections(st, inv, file, conf):
    """
    Process spectrogram from stream and inventory, save results in file
    """
    st0 = st.copy()
    start = jday2UTCDateTime(parse_jday_from_string(file))
    end = start + 86400
    st0.trim(start, end)
    if len(st0) > 0:
        fs = st0[0].stats.sampling_rate
        if conf['detections']['freqmax']*2 > fs:
            st0.filter('highpass',
                       freq=conf['detections']['freqmin'],
                       corners=conf['detections']['corners'])
        else:
            st0.filter('bandpass',
                       freqmin=conf['detections']['freqmin'],
                       freqmax=conf['detections']['freqmax'],
                       corners=conf['detections']['corners'])
        # st0.merge()
        sta = int(conf['detections']['sta']*fs)
        lta = int(conf['detections']['lta']*fs)
        max_length = int(conf['detections']['max_length']*fs)
        trigs = np.zeros(24)
        for tr in st0:
            starthour = (tr.stats.starttime - start)/3600
            if tr.data.size > lta+max_length:
                cft = classic_sta_lta(tr.data, sta, lta)
                on_of = trigger_onset(cft,
                                      conf['detections']['on_threshold'],
                                      conf['detections']['off_threshold'],
                                      max_len=max_length)
                on_of = starthour + np.divide(on_of, 3600*fs)
                for trig in on_of:
                    trigs[int(trig[0])] += 1
                np.savez_compressed(file, triggers=trigs)
            else:
                print("%s: not enough data available" % (file.split('/')[-1]))
    else:
        print("%s: no [meta]data available" % (file.split('/')[-1]))
